# Android RTP #

A foundation for an Android RTP video chat application.

### What is this repository for? ###

* This repository is for the development and continued improvement for solid foundation of an Android video chat app using RTP.
* Version 0.01


### How do I get set up? ###

* Create a clone of the repository locally (Android Studio or Source Tree work great).
* Open the repository up in Android Studio (1.5.1 is the current version as of writing this).
* Run the Gradle Build function in Android Studio


### Contribution guidelines ###

* https://androidrtp.slack.com/
* https://trello.com/b/fHdvc4z0/android-rtp
* Code should be using all current APIs with support libraries (support for API 16 and newer as of writing this).

### Who do I talk to? ###

* john.weland@gmail.com
